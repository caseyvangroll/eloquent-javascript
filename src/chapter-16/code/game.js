import levels from './levels.js'

class Level {
  constructor (plan) {
    const rows = plan.trim().split('\n').map(l => [...l])
    this.height = rows.length
    this.width = rows[0].length
    this.startActors = []
    this.startTextFns = [(state) => ({
      x: 1,
      y: 0,
      text: `Lives left: ${state.livesLeft}`
    }), (state) => ({
      x: 1,
      y: 2,
      text: state.status === 'paused' ? 'Paused' : ''
    })]
    this.startLivesLeft = 3

    this.rows = rows.map((row, y) => {
      return row.map((ch, x) => {
        const type = levelChars[ch]
        if (typeof type === 'string') return type
        this.startActors.push(
          type.create(new Vec(x, y), ch))
        return 'empty'
      })
    })
  }

  touches (pos, size, type) {
    const xStart = Math.floor(pos.x)
    const xEnd = Math.ceil(pos.x + size.x)
    const yStart = Math.floor(pos.y)
    const yEnd = Math.ceil(pos.y + size.y)

    for (let y = yStart; y < yEnd; y++) {
      for (let x = xStart; x < xEnd; x++) {
        const isOutside = x < 0 || x >= this.width ||
                        y < 0 || y >= this.height
        const here = isOutside ? 'wall' : this.rows[y][x]
        if (here === type) return true
      }
    }
    return false
  }
}

class State {
  constructor (level, actors, textFns, status, livesLeft) {
    this.level = level
    this.actors = actors
    this.textFns = textFns
    this.status = status
    this.livesLeft = livesLeft
  }

  static start (level) {
    return new State(level, level.startActors, level.startTextFns, 'playing', level.startLivesLeft)
  }

  get player () {
    return this.actors.find(a => a.type === 'player')
  }

  update (time, keys) {
    if (keys.Escape) {
      if (this.status === 'paused') {
        return new State(this.level, this.actors, this.textFns, 'playing', this.livesLeft)
      } else if (this.status === 'playing') {
        return new State(this.level, this.actors, this.textFns, 'paused', this.livesLeft)
      }
    }

    if (this.status === 'paused') {
      return this
    }

    const actors = this.actors
      .map(actor => actor.update(time, this, keys))
    let newState = new State(this.level, actors, this.textFns, this.status, this.livesLeft)

    if (newState.status !== 'playing') return newState

    const player = newState.player
    if (this.level.touches(player.pos, player.size, 'lava')) {
      if (this.livesLeft > 1) {
        return new State(this.level, this.level.startActors, this.level.startTextFns, 'playing', this.livesLeft - 1)
      }
      return new State(this.level, actors, this.textFns, 'lost', 0)
    }

    for (const actor of actors) {
      if (actor !== player && overlap(actor, player)) {
        newState = actor.collide(newState)
      }
    }
    return newState
  }
}

class Vec {
  constructor (x, y) {
    this.x = x; this.y = y
  }

  plus (other) {
    return new Vec(this.x + other.x, this.y + other.y)
  }

  times (factor) {
    return new Vec(this.x * factor, this.y * factor)
  }
}

const playerXSpeed = 7
const gravity = 30
const jumpSpeed = 17
class Player {
  constructor (pos, speed) {
    this.pos = pos
    this.speed = speed
    this.size = new Vec(0.8, 1.5)
  }

  get type () { return 'player' }

  static create (pos) {
    return new Player(pos.plus(new Vec(0, -0.5)),
      new Vec(0, 0))
  }

  update (time, state, keys) {
    let xSpeed = 0
    if (keys.ArrowLeft) xSpeed -= playerXSpeed
    if (keys.ArrowRight) xSpeed += playerXSpeed
    let pos = this.pos
    const movedX = pos.plus(new Vec(xSpeed * time, 0))
    if (!state.level.touches(movedX, this.size, 'wall')) {
      pos = movedX
    }

    let ySpeed = this.speed.y + time * gravity
    const movedY = pos.plus(new Vec(0, ySpeed * time))
    if (!state.level.touches(movedY, this.size, 'wall')) {
      pos = movedY
    } else if (keys.ArrowUp && ySpeed > 0) {
      ySpeed = -jumpSpeed
    } else {
      ySpeed = 0
    }
    return new Player(pos, new Vec(xSpeed, ySpeed))
  }
}

class Lava {
  constructor (pos, speed, reset) {
    this.pos = pos
    this.speed = speed
    this.reset = reset
    this.size = new Vec(1, 1)
  }

  get type () { return 'lava' }

  static create (pos, ch) {
    if (ch === '=') {
      return new Lava(pos, new Vec(2, 0))
    } else if (ch === '|') {
      return new Lava(pos, new Vec(0, 2))
    } else if (ch === 'v') {
      return new Lava(pos, new Vec(0, 3), pos)
    }
  }

  collide (state) {
    if (state.livesLeft > 1) {
      return new State(state.level, state.level.startActors, state.level.startTextFns, 'playing', state.livesLeft - 1)
    }
    return new State(state.level, state.actors, state.textFns, 'lost', 0)
  }

  update (time, state) {
    const newPos = this.pos.plus(this.speed.times(time))
    if (!state.level.touches(newPos, this.size, 'wall')) {
      return new Lava(newPos, this.speed, this.reset)
    } else if (this.reset) {
      return new Lava(this.reset, this.speed, this.reset)
    } else {
      return new Lava(this.pos, this.speed.times(-1))
    }
  }
}

const wobbleSpeed = 8
const wobbleDist = 0.07
class Coin {
  constructor (pos, basePos, wobble) {
    this.pos = pos
    this.basePos = basePos
    this.wobble = wobble
    this.size = new Vec(0.6, 0.6)
  }

  get type () { return 'coin' }

  static create (pos) {
    const basePos = pos.plus(new Vec(0.2, 0.1))
    return new Coin(basePos, basePos,
      Math.random() * Math.PI * 2)
  }

  collide (state) {
    const filtered = state.actors.filter(a => a !== this)
    let status = state.status
    if (!filtered.some(a => a.type === 'coin')) status = 'won'
    return new State(state.level, filtered, state.textFns, status, state.livesLeft)
  }

  update (time) {
    const wobble = this.wobble + time * wobbleSpeed
    const wobblePos = Math.sin(wobble) * wobbleDist
    return new Coin(this.basePos.plus(new Vec(0, wobblePos)),
      this.basePos, wobble)
  }
}

class Monster {
  constructor (pos, speed) {
    this.pos = pos
    this.speed = speed
    this.size = new Vec(1.2, 2)
  }

  get type () { return 'monster' }

  static create (pos) {
    return new Monster(pos.plus(new Vec(0, -1)), new Vec(1, 0))
  }

  update (time, state) {
    const newPos = this.pos.plus(this.speed.times(time))
    if (!state.level.touches(newPos, this.size, 'wall')) {
      return new Monster(newPos, this.speed, this.reset)
    } else if (this.reset) {
      return new Monster(this.reset, this.speed, this.reset)
    } else {
      return new Monster(this.pos, this.speed.times(-1))
    }
  }

  collide (state) {
    const playerBottomY = state.player.pos.y + state.player.size.y
    const monsterTopY = this.pos.y
    const isPlayerOnHead = Math.abs(playerBottomY - monsterTopY) < 0.1

    if (isPlayerOnHead) {
      return new State(state.level, state.actors.filter(actor => actor !== this), state.textFns, state.status, state.livesLeft)
    }

    if (state.livesLeft > 1) {
      return new State(state.level, state.level.startActors, state.level.startTextFns, 'playing', state.livesLeft - 1)
    }
    return new State(state.level, state.actors, state.textFns, 'lost', 0)
  }
}

const levelChars = {
  '.': 'empty',
  '#': 'wall',
  '+': 'lava',
  '@': Player,
  o: Coin,
  '=': Lava,
  '|': Lava,
  v: Lava,
  M: Monster
}

function elt (name, attrs, ...children) {
  const dom = document.createElement(name)
  for (const attr of Object.keys(attrs)) {
    dom.setAttribute(attr, attrs[attr])
  }
  for (const child of children) {
    dom.appendChild(child)
  }
  return dom
}

function drawGrid (level) {
  return elt('table', {
    class: 'background',
    style: `width: ${level.width * scale}px`
  }, ...level.rows.map(row =>
    elt('tr', { style: `height: ${scale}px` },
      ...row.map(type => elt('td', { class: type })))
  ))
}

function drawActors (actors) {
  return elt('div', {}, ...actors.map(actor => {
    const rect = elt('div', { class: `actor ${actor.type}` })
    rect.style.width = `${actor.size.x * scale}px`
    rect.style.height = `${actor.size.y * scale}px`
    rect.style.left = `${actor.pos.x * scale}px`
    rect.style.top = `${actor.pos.y * scale}px`
    return rect
  }))
}

function drawTextLayer (state) {
  const texts = state.textFns.map(textFn => textFn(state))
  return elt('div', {}, ...texts.map(({ className, text, x, y, fontSize }) => {
    const el = elt('p', { class: `text ${className}` }, document.createTextNode(text))
    el.style.left = `${x * scale}px`
    el.style.top = `${y * scale}px`
    el.style.pointerEvents = 'none'
    if (fontSize) {
      el.style.fontSize = fontSize
    }
    return el
  }))
}

const scale = 20

export class DOMDisplay {
  constructor (parent, level) {
    this.dom = elt('div', { class: 'game' }, drawGrid(level))
    this.actorLayer = null
    this.textLayer = null
    parent.appendChild(this.dom)
  }

  clear () { this.dom.remove() }

  syncState (state) {
    this.actorLayer?.remove()
    this.actorLayer = drawActors(state.actors)
    this.dom.appendChild(this.actorLayer)

    this.textLayer?.remove()
    this.textLayer = drawTextLayer(state)
    this.dom.parentElement.appendChild(this.textLayer)

    this.dom.className = `game ${state.status}`
    this.scrollPlayerIntoView(state)
  }

  scrollPlayerIntoView (state) {
    const width = this.dom.clientWidth
    const height = this.dom.clientHeight
    const margin = width / 3

    // The viewport
    const left = this.dom.scrollLeft; const right = left + width
    const top = this.dom.scrollTop; const bottom = top + height

    const player = state.player
    const center = player.pos.plus(player.size.times(0.5))
      .times(scale)

    if (center.x < left + margin) {
      this.dom.scrollLeft = center.x - margin
    } else if (center.x > right - margin) {
      this.dom.scrollLeft = center.x + margin - width
    }
    if (center.y < top + margin) {
      this.dom.scrollTop = center.y - margin
    } else if (center.y > bottom - margin) {
      this.dom.scrollTop = center.y + margin - height
    }
  }
}

function overlap (actor1, actor2) {
  return actor1.pos.x + actor1.size.x > actor2.pos.x &&
         actor1.pos.x < actor2.pos.x + actor2.size.x &&
         actor1.pos.y + actor1.size.y > actor2.pos.y &&
         actor1.pos.y < actor2.pos.y + actor2.size.y
}

let arrowKeys

function subscribeToKeyPresses () {
  const keys = ['ArrowLeft', 'ArrowRight', 'ArrowUp', 'Escape']
  const down = Object.create(null)
  function track (event) {
    if (keys.includes(event.key)) {
      down[event.key] = event.type === 'keydown'
      event.preventDefault()
    }
  }
  window.addEventListener('keydown', track)
  window.addEventListener('keyup', track)
  arrowKeys = down
  return () => {
    window.removeEventListener('keydown', track)
    window.removeEventListener('keyup', track)
  }
}

function runAnimation (frameFunc) {
  let lastTime = null
  function frame (time) {
    if (lastTime != null) {
      const timeStep = Math.min(time - lastTime, 100) / 1000
      if (frameFunc(timeStep) === false) return
    }
    lastTime = time
    requestAnimationFrame(frame)
  }
  requestAnimationFrame(frame)
}

function runLevel (level, Display) {
  const display = new Display(document.body, level)
  let state = State.start(level)
  let ending = 1
  return new Promise(resolve => {
    runAnimation(time => {
      state = state.update(time, arrowKeys)
      display.syncState(state)
      if (['playing', 'paused'].includes(state.status)) {
        return true
      } else if (ending > 0) {
        ending -= time
        return true
      } else {
        display.clear()
        resolve(state.status)
        return false
      }
    })
  })
}

export async function runGame () {
  const unsubscribe = subscribeToKeyPresses()
  for (let level = 0; level < levels.length;) {
    const status = await runLevel(new Level(levels[level]),
      DOMDisplay)
    if (status === 'won') level++
  }
  console.log("You've won!")
  unsubscribe()
}
