
import { getRandomElement } from './helpers.js'
import { VillageState } from './village-state.js'

/* Robot that wanders aimlessly */
export function getRandomRobotAction (villageState, robotMemory) {
  const { roadGraph, robotLocation } = villageState
  const adjacentLocations = roadGraph[robotLocation]
  return {
    robotName: 'random robot',
    nextRobotLocation: getRandomElement(adjacentLocations),
    nextRobotMemory: robotMemory
  }
}

/* Robot that always follows a mail route */
export function getMailRouteRobotAction (villageState, robotMemory) {
  if (robotMemory.length === 0) {
    robotMemory = [
      "Alice's House", 'Cabin', "Alice's House", "Bob's House",
      'Town Hall', "Daria's House", "Ernie's House",
      "Grete's House", 'Shop', "Grete's House", 'Farm',
      'Marketplace', 'Post Office'
    ]
  }

  return {
    robotName: 'mail route robot',
    nextRobotLocation: robotMemory[0],
    nextRobotMemory: robotMemory.slice(1)
  }
}

/* Robot that always delivers the parcel at the head of the parcels array next */
export function getGoalOrientedRobotAction (villageState, robotMemory) {
  const { roadGraph, robotLocation, parcels } = villageState
  let route = robotMemory

  if (route.length === 0) {
    const parcel = parcels[0]
    const isRobotHoldingParcel = parcel.currentLocation === robotLocation
    if (isRobotHoldingParcel) {
      route = findRoute({
        roadGraph,
        source: robotLocation,
        destination: parcel.destination
      })
    } else {
      route = findRoute({
        roadGraph,
        source: robotLocation,
        destination: parcel.currentLocation
      })
    }
  }

  return {
    robotName: 'goal-oriented robot',
    nextRobotLocation: route[0],
    nextRobotMemory: route.slice(1)
  }
}

function findRoute ({ roadGraph, source, destination }) {
  const possibleRoutes = [{ currentLocation: source, route: [] }]

  // Breadth-first search
  for (let i = 0; i < possibleRoutes.length; i++) {
    const { currentLocation, route } = possibleRoutes[i]
    for (const adjacentLocation of roadGraph[currentLocation]) {
      if (adjacentLocation === destination) {
        return route.concat(adjacentLocation)
      }
      // If no other possible route is already at this location then
      // this one got there first and would be the shortest path to exploring it.
      if (!possibleRoutes.some(possibleRoute => possibleRoute.currentLocation === adjacentLocation)) {
        possibleRoutes.push({
          currentLocation: adjacentLocation,
          route: route.concat(adjacentLocation)
        })
      }
    }
  }
}

export function runRobot ({
  getRobotAction,
  villageState = VillageState.random(),
  robotMemory = []
}) {
  let debugInfo
  for (let turn = 0; ; turn++) {
    if (villageState.parcels.length === 0) {
      debugInfo += `\n\nDone in ${turn} turns\n`
      return {
        turn,
        debugInfo
      }
    }
    const robotAction = getRobotAction(villageState, robotMemory)
    const nextVillageState = villageState.unloadParcelsAndMoveRobot(robotAction.nextRobotLocation)
    robotMemory = robotAction.nextRobotMemory
    if (!debugInfo) {
      debugInfo = `Running ${robotAction.robotName}...\n\n${villageState.robotLocation}`
    }
    debugInfo += ` -> ${nextVillageState.robotLocation}`
    villageState = nextVillageState
  }
}

export function compareRobots ({
  getRobotOneAction,
  robotOneMemory,
  getRobotTwoAction,
  robotTwoMemory,
  runCount = 100
}) {
  const robotOneSolutionTimes = []
  const robotTwoSolutionTimes = []

  for (let i = 0; i < runCount; i += 1) {
    const villageState = VillageState.random()
    robotOneSolutionTimes.push(runRobot({
      getRobotAction: getRobotOneAction,
      villageState,
      robotMemory: robotOneMemory
    }).turn)
    robotTwoSolutionTimes.push(runRobot({
      getRobotAction: getRobotTwoAction,
      villageState,
      robotMemory: robotTwoMemory
    }).turn)
  }

  const robotOneAvgSolutionTime = robotOneSolutionTimes.reduce((acc, n) => acc + n, 0) / runCount
  const robotTwoAvgSolutionTime = robotTwoSolutionTimes.reduce((acc, n) => acc + n, 0) / runCount

  console.log(`Robot One Average Solution Time: ${robotOneAvgSolutionTime}`)
  console.log(`Robot Two Average Solution Time: ${robotTwoAvgSolutionTime}`)
}
