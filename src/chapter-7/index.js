import { getRandomRobotAction, getMailRouteRobotAction, getGoalOrientedRobotAction, runRobot, compareRobots } from './robots.js'
import { PGroup } from './p-group.js'

// ---
// Robot
// ---

console.log(runRobot({ getRobotAction: getRandomRobotAction }).debugInfo)

console.log('Running mail route robot...')
console.log(runRobot({ getRobotAction: getMailRouteRobotAction }).debugInfo)

console.log('Running goal-oriented robot...')
console.log(runRobot({ getRobotAction: getGoalOrientedRobotAction }).debugInfo)

console.log('Running comparison of mail route robot and goal-oriented robot...')
compareRobots({
  getRobotOneAction: getMailRouteRobotAction,
  robotOneMemory: [],
  getRobotTwoAction: getGoalOrientedRobotAction,
  robotTwoMemory: [],
  runCount: 10000
})

// ---
// P-Group
// ---

const a = PGroup.empty.add('a')
const ab = a.add('b')
const b = ab.delete('a')

console.log(b.has('b'))
// → true
console.log(a.has('b'))
// → false
console.log(b.has('a'))
// → false
