export function buildRoadGraph (roads) {
  const roadGraph = {}
  for (const [locationA, locationB] of roads.map(road => road.split('-'))) {
    roadGraph[locationA] ||= []
    roadGraph[locationB] ||= []
    roadGraph[locationA].push(locationB)
    roadGraph[locationB].push(locationA)
  }
  return roadGraph
}

export function getRandomElement (array) {
  const choice = Math.floor(Math.random() * array.length)
  return array[choice]
}
