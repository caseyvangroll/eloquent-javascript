export class PGroup {
  constructor (itemsMap) {
    this.itemsMap = itemsMap
  }

  add (item) {
    return new PGroup({
      ...this.itemsMap,
      [item]: true
    })
  }

  delete (item) {
    const { [item]: removedItem, ...rest } = this.itemsMap
    return new PGroup(rest)
  }

  has (value) {
    return this.itemsMap[value] !== undefined
  }

  static from (itemsIterable) {
    const itemsMap = [...itemsIterable].reduce((acc, item) => ({ ...acc, [item]: true }), {})
    return new PGroup(itemsMap)
  }

  static empty = PGroup.from([])
}
