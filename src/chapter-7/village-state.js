import { buildRoadGraph, getRandomElement } from './helpers.js'

export const roads = [
  "Alice's House-Bob's House", "Alice's House-Cabin",
  "Alice's House-Post Office", "Bob's House-Town Hall",
  "Daria's House-Ernie's House", "Daria's House-Town Hall",
  "Ernie's House-Grete's House", "Grete's House-Farm",
  "Grete's House-Shop", 'Marketplace-Farm',
  'Marketplace-Post Office', 'Marketplace-Shop',
  'Marketplace-Town Hall', 'Shop-Town Hall'
]

export class VillageState {
  constructor ({ roadGraph, robotLocation, parcels }) {
    this.roadGraph = roadGraph
    this.robotLocation = robotLocation
    this.parcels = parcels
  }

  unloadParcelsAndMoveRobot (nextRobotLocation) {
    if (!this.roadGraph[this.robotLocation].includes(nextRobotLocation)) {
      console.error(`Tried to move robot from ${this.robotLocation} to ${nextRobotLocation} but no road connects those two places.`)
      return this
    } else {
      const nextParcels = this.parcels.map(parcel => {
        if (parcel.currentLocation !== this.robotLocation) {
          return parcel
        }
        return {
          currentLocation: nextRobotLocation,
          destination: parcel.destination
        }
      }).filter(parcel => parcel.currentLocation !== parcel.destination)

      return new VillageState({
        roadGraph: this.roadGraph,
        robotLocation: nextRobotLocation,
        parcels: nextParcels
      })
    }
  }

  static random (parcelCount = 5) {
    const roadGraph = buildRoadGraph(roads)

    const parcels = []
    for (let i = 0; i < parcelCount; i++) {
      const locationA = getRandomElement(Object.keys(roadGraph))
      let locationB
      do {
        locationB = getRandomElement(Object.keys(roadGraph))
      } while (locationA === locationB)
      parcels.push({
        currentLocation: locationA,
        destination: locationB
      })
    }

    return new VillageState({
      roadGraph,
      robotLocation: 'Post Office',
      parcels
    })
  }
}
