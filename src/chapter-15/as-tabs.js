function createContainer () {
  const container = document.createElement('div')
  container.className = 'container'
  return container
}

function createTopBar () {
  const topBar = document.createElement('div')
  topBar.className = 'top-bar'
  return topBar
}

function getTabName (node, i) {
  return node.children[i].dataset.tabname || `unknown ${i}`
}
function createTab () {
  const tab = document.createElement('div')
  tab.className = 'tab'
  return tab
}

export default function asTabs (node) {
  const container = createContainer()
  const topBar = createTopBar()
  let selectedTab, selectedTabName

  for (let i = 0; i < node.children.length; i += 1) {
    const tab = createTab()
    const tabName = getTabName(node, i)
    tab.textContent = tabName

    const unhideThisTabAndItsContent = () => {
      const tabContent = document.querySelector(`[data-tabname="${tabName}"]`)
      tabContent.style.display = 'block'

      // update this tab's header styling
      tab.className = 'tab selected'

      // store refs for next guy
      selectedTab = tab
      selectedTabName = tabName
    }

    tab.addEventListener('click', () => {
      // hide stuff that was selected before
      const selectedContent = document.querySelector(
        `[data-tabname="${selectedTabName}"]`
      )
      selectedContent.style.display = 'none'
      // update other tab's header styling
      selectedTab.className = 'tab'

      unhideThisTabAndItsContent()
    })

    // Style each tab
    if (!selectedTabName) {
      unhideThisTabAndItsContent()
    } else {
      node.children[i].style.display = 'none'
    }
    topBar.appendChild(tab)
  }
  node.parentNode.appendChild(container)
  container.appendChild(topBar)
  container.appendChild(node)
}
