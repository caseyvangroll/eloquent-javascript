// Fill in the regular expressions

verify(/ca[rt]/,
  ['my car', 'bad cats'],
  ['camper', 'high art'])

verify(/ /,
  ['pop culture', 'mad props'],
  ['plop', 'prrrop'])

verify(/rr[^u]/,
  ['ferret', 'ferry', 'ferrari'],
  ['ferrum', 'transfer A'])

verify(/ /,
  ['how delicious', 'spacious room'],
  ['ruinous', 'consciousness'])

verify(/\./,
  ['bad punctuation .'],
  ['escape the period'])

verify(/\S{6,}/,
  ['Siebentausenddreihundertzweiundzwanzig'],
  ['no', 'three small words'])

verify(/.*[do].* .*[ln].*/,
  ['red platypus', 'wobbling nest'],
  ['earth bed', 'learning ape', 'BEET'])

function verify (regexp, yes, no) {
  // Ignore unfinished exercises
  if (regexp.source === '...') return
  for (const str of yes) {
    if (!regexp.test(str)) {
      console.log(`Failure to match '${str}'`)
    } else {
      console.log(`Successfully matched '${str}'`)
    }
  }
  for (const str of no) {
    if (regexp.test(str)) {
      console.log(`Unexpected match for '${str}'`)
    }
  }
}

// Exercise 2

const text = "'I'm the cook,' he said, 'it's my job.'"
// Change this call.
console.log(text.replace(/\B'/g, '"'))
// → "I'm the cook," he said, "it's my job."

// Exercise 3

// Fill in this regular expression.
const number = /^\.$/

// Tests:
for (const str of ['1', '-1', '+15', '1.55', '.5', '5.',
  '1.3e2', '1E-4', '1e+12']) {
  if (!number.test(str)) {
    console.log(`Failed to match '${str}'`)
  }
}
for (const str of ['1a', '+-1', '1.2.3', '1+1', '1e4.5',
  '.5.', '1f5', '.']) {
  if (number.test(str)) {
    console.log(`Incorrectly accepted '${str}'`)
  }
}
