/*
Build an interface that allows people to type and run pieces of JavaScript code.

Put a button next to a <textarea> field that, when pressed, uses the Function
constructor we saw in Chapter 10 to wrap the text in a function and call it.
Convert the return value of the function, or any error it raises, to a string
and display it below the text field.
*/

const textArea = document.querySelector('#code')
const button = document.querySelector('#button')
const output = document.querySelector('#output')

function handleClick () {
  let result
  try {
    const runCode = Function('', textArea.value)
    result = runCode()
  } catch (e) {
    result = String(e)
  }
  output.innerText = result
}

button.addEventListener('click', handleClick)
