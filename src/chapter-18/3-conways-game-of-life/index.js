/*
Conway’s Game of Life is a simple simulation that creates artificial “life”
on a grid, each cell of which is either alive or not. Each generation (turn),
the following rules are applied:

- Any live cell with fewer than two or more than three live neighbors dies.
- Any live cell with two or three live neighbors lives on to the next generation.
- Any dead cell with exactly three live neighbors becomes a live cell.

A neighbor is defined as any adjacent cell, including diagonally adjacent ones.

Note that these rules are applied to the whole grid at once, not one square at a time.
That means the counting of neighbors is based on the situation at the start of the generation,
and changes happening to neighbor cells during this generation should not influence the
new state of a given cell.

Implement this game using whichever data structure you find appropriate.
Use Math.random to populate the grid with a random pattern initially.
Display it as a grid of checkbox fields, with a button next to it to advance to the next generation.
When the user checks or unchecks the checkboxes, their changes should be included when computing the next generation.
*/

const WIDTH = 10
const HEIGHT = 10
let button
let grid

const isValidCellLocation = (x, y) => (
  x >= 0 && x < WIDTH &&
  y >= 0 && y < HEIGHT
)
const getNeighbors = (cells, i) => {
  const cellX = i % WIDTH
  const cellY = Math.floor(i / WIDTH)
  const neighbors = []

  for (let y = cellY - 1; y <= cellY + 1; y += 1) {
    for (let x = cellX - 1; x <= cellX + 1; x += 1) {
      const isCenterCell = x === cellX && y === cellY
      if (!isCenterCell && isValidCellLocation(x, y)) {
        const neighbor = cells[y * WIDTH + x]
        neighbors.push(neighbor)
      }
    }
  }

  return neighbors
}

export const getNextCellValue = ({
  isAlive,
  liveNeighborCt
}) => {
  if (isAlive) {
    return liveNeighborCt >= 2 && liveNeighborCt <= 3
  }
  return liveNeighborCt === 3
}

function handleClick () {
  const cells = [...grid.children]
  cells.map((cell, i) => {
    const neighbors = getNeighbors(cells, i)

    cell.dataset.nextChecked = getNextCellValue({
      isAlive: cell.checked,
      liveNeighborCt: neighbors.reduce((acc, neighbor) => {
        return acc + (neighbor?.checked ? 1 : 0)
      }, 0)
    })

    return cell
  }).forEach((cell) => {
    cell.checked = JSON.parse(cell.dataset.nextChecked)
    delete cell.dataset.nextChecked
  })
}

function initializeGrid (width, height) {
  grid.style.gridTemplateRows = `repeat(${height}, 20px)`
  grid.style.gridTemplateColumns = `repeat(${width}, 20px)`
  for (let y = 0; y < height; y += 1) {
    for (let x = 0; x < width; x += 1) {
      const checkbox = document.createElement('input')
      checkbox.type = 'checkbox'
      checkbox.style.margin = '0'
      checkbox.style.borderRadius = '0px'
      grid.appendChild(checkbox)
    }
  }
}

export function initialize () {
  button = document.querySelector('#next')
  grid = document.querySelector('#grid')
  initializeGrid(WIDTH, HEIGHT)
  button.addEventListener('click', handleClick)
}
