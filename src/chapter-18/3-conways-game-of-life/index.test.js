import { describe, it } from 'node:test'
import assert from 'assert'
import { getNextCellValue } from './index.js'

describe('getNextCellValue', () => {
  describe('Any live cell with fewer than two or more than three live neighbors dies.', () => {
    it('one neighbor', () => {
      const actual = getNextCellValue({
        isAlive: true,
        liveNeighborCt: 1
      })
      const expected = false
      assert.strictEqual(actual, expected)
    })

    it('four neighbors', () => {
      const actual = getNextCellValue({
        isAlive: true,
        liveNeighborCt: 4
      })
      const expected = false
      assert.strictEqual(actual, expected)
    })
  })
  describe('Any live cell with two or three live neighbors lives on to the next generation.', () => {
    it('two neighbors', () => {
      const actual = getNextCellValue({
        isAlive: true,
        liveNeighborCt: 2
      })
      const expected = true
      assert.strictEqual(actual, expected)
    })
    it('three neighbors', () => {
      const actual = getNextCellValue({
        isAlive: true,
        liveNeighborCt: 3
      })
      const expected = true
      assert.strictEqual(actual, expected)
    })
  })
  it('Any dead cell with exactly three live neighbors becomes a live cell.', () => {
    const actual = getNextCellValue({
      isAlive: false,
      liveNeighborCt: 3
    })
    const expected = true
    assert.strictEqual(actual, expected)
  })
})
