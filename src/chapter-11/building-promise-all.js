function myPromiseAll (promises) {
  return new Promise((resolve, reject) => {
    console.log(promises)
    async function resolvePromises () {
      const results = []
      for (let i = 0; i < promises.length; i += 1) {
        const promise = promises[i]
        const result = await promise
        results.push(result)
      }
      return results
    }

    resolvePromises().then(resolve).catch(reject)
  })
}

// Test code.
myPromiseAll([]).then(array => {
  console.log('This should be []:', array)
})
function soon (val) {
  return new Promise(resolve => {
    setTimeout(() => resolve(val), Math.random() * 500)
  })
}
myPromiseAll([soon(1), soon(2), soon(3)]).then(array => {
  console.log('This should be [1, 2, 3]:', array)
})
myPromiseAll([soon(1), Promise.reject(new Error('X')), soon(3)])
  .then(array => {
    console.log('We should not get here')
  })
  .catch(error => {
    if (error !== 'X') {
      console.log('Unexpected failure:', error)
    }
  })
